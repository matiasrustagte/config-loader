module.exports = {
  port: 80,
  db: {
    host: 'localhost',
    port: 27017,
    name: 'app',
  }
};