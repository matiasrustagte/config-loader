const path = require('path');
const fs = require('fs');
const cmdToJson = require('@agte/cmd-to-json');
const assign = require('mixin-deep');

module.exports = function loadConfig (dir, customConfig) {
  if (!dir)
    throw Error('Config directory is not defined.');
  dir = path.resolve(dir);
  if (!fs.existsSync(dir))
    throw Error('Config directory does not exists.');

  const args = cmdToJson();
  const config = {};

  // Настройки по умолчанию.
  const defaultConfig = dir + '/default.js';
  if (fs.existsSync(defaultConfig))
    assign(config, require(defaultConfig));

  // Настройки, зависящие от режима запуска.
  config.environment = process.env.NODE_ENV || args.NODE_ENV || args.env || 'development';
  delete config.NODE_ENV;
  const envConfig = dir + `/${config.environment}.js`;
  if (fs.existsSync(envConfig))
    assign(config, require(envConfig));

  // Кастомные настройки этого экземпляра приложения.
  if (customConfig) {
    customConfig = path.normalize(customConfig);
    if (fs.existsSync(customConfig))
      assign(config, require(customConfig));
  }

  // Настройки из командной строки.
  assign(config, args);

  return config;
};